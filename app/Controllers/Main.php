<?php namespace App\Controllers;

use \App\Models\Model_Detail;

class Main extends BaseController
{
    protected $detail_model;

    public function __construct()
    {
        $this->detail_model = new Model_Detail();
    }

	public function index()
	{
        $item = $this->detail_model->paginate(8, 'tabel_detail');

        $data = [
            'items' => $item,
        ];

		return view('/main/afterlogin', $data);
    }
    
    public function explore()
    {
        return view('/main/searchpage');
    }

    public function livestream()
    {
        return view('/main/liveStream');
    }

    public function schoolProfile()
    {
        return view('/main/schoolProfile');
    }
}

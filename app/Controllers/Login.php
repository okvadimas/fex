<?php namespace App\Controllers;

use \App\Models\Model_User;

class Login extends BaseController
{
    protected $user_model;

    public function __construct()
    {
        $this->user_model = new Model_User();
    }

	public function login()
	{
        $encrypter = \Config\Services::encrypter();
        
        $user = $this->user_model->where(['username' => $this->request->getVar('email/username/')])->orWhere(['email', $this->request->getVar('email/username')])->first();

        if(isset($user))
        {
            if(password_verify($this->request->getVar('password'), $user['password']))
            {
                session()->setFlashdata('pesan', 'Success Login as ' . $user["username"]);
                return redirect()->to('/main/');
            } else {
                $validation = \Config\Services::validation();
                session()->setFlashdata('pesan_gagal', 'Invalid Username or Password !');
                return redirect()->to('/home/login')->withInput()->with('validation', $validation);
            };
        } else {
            $validation = \Config\Services::validation();
            session()->setFlashdata('pesan_gagal', 'Invalid Username or Password !');
            return redirect()->to('/home/login')->withInput()->with('validation', $validation);
        };
    }

    public function register()
    {
        $encrypter = \Config\Services::encrypter();

        if(!$this->validate([
            'username' => 'required|is_unique[user.username]',
            'email' => 'required|is_unique[user.email]',
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata('pesan_gagal', 'Username or Email Already Exist !');
            return redirect()->to('/home/register')->withInput()->with('validation', $validation);
        };

        if(!$this->validate([
            'password1' => 'required',
            'password2' => 'required|matches[password1]',
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata("pesan_gagal", "Password and Confirmation Password Doesn't Match !");
            return redirect()->to('home/register')->withInput()->with('validation', $validation);
        };

        $this->user_model->save([
            'username' => $this->request->getVar('username'),
            'email' => $this->request->getVar('email'),
            'password' => password_hash($this->request->getVar('password2'), PASSWORD_DEFAULT),
        ]);

        session()->setFlashdata("pesan", 'Success Create New Account !');
        return redirect()->to('/home/login');
    }
}

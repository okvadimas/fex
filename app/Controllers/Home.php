<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		return view('/home/index');
	}

	public function login()
	{
		return view('/home/login');
	}

	public function register()
	{
		return view('/home/registrasi');
	}
}

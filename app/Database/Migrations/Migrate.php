<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddBlog extends Migration
{

        public function up()
        {
                $this->forge->addField([
                        'id'          => [
                                'type'           => 'INT',
                                'auto_increment' => true,
                        ],
                        'nama_sekolah'       => [
                                'type'           => 'VARCHAR',
                                'constraint'     => '255',
                        ],
                        'desc' => [
                                'type'           => 'VARCHAR',
                                'constraint'     => '255',
                        ],
                        'alamat' => [
                            'type'           => 'VARCHAR',
                            'constraint'     => '255',
                        ],
                        'fasilitas' => [
                            'type'           => 'VARCHAR',
                            'constraint'     => '255',
                        ],
                        'ig_stream' => [
                            'type'           => 'VARCHAR',
                            'constraint'     => '255',
                        ],
                        'yt_stream' => [
                            'type'           => 'VARCHAR',
                            'constraint'     => '255',
                        ],
                        'zoom_stream' => [
                            'type'           => 'VARCHAR',
                            'constraint'     => '255',
                        ],
                        'prestasi' => [
                            'type'           => 'VARCHAR',
                            'constraint'     => '255',
                    ],
                ]);
                $this->forge->addKey('id', true);
                $this->forge->createTable('detail');
        }

        public function down()
        {
                $this->forge->dropTable('detail');
        }
}
<?php namespace App\Database\Seeds;

class Seeder extends \CodeIgniter\Database\Seeder
{
        public function run()
        {
            for ($i = 1; $i <= 30; $i++) {
                $data = [
                        'nama_sekolah' => 'Sekolah Negeri ' . $i,
                        'desc' => 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor',
                        'alamat' => 'Jalan Sekolah Negeri ' . $i,
                        'fasilitas' => 'Fasilitas Sekolah Negeri ' . $i,
                        'ig_stream' => 'Instagram Stream Sekolah Negeri ' . $i,
                        'yt_stream' => 'Youtube Stream Sekolah Negeri ' . $i,
                        'zoom_stream' => 'Zoom Stream Sekolah Negeri ' . $i,
                        'prestasi' => 'Prestasi Sekolah Negeri ' . $i,
                ];

                $this->db->table('detail')->insert($data);
            };


                // Simple Queries
                // $this->db->query("INSERT INTO detail (username, email) VALUES(:username:, :email:)",
                //         $data
                // );

                // Using Query Builder
                // $this->db->table('detail')->insert($data);
        }
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>php echo school name</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/liveStream.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
</head>
<body>
    <section id="nav">
    <nav class="navbar navbar-expand navbar-light bg-white">
        <div class="container-xl d-flex justify-content-between">
        <a class="navbar-brand d-none d-sm-block h1 my-0 font-weight-bold" href="#">Halmahera Music School</a>
        <a class="navbar-brand d-block d-sm-none h my-0 font-weight-bold" href="#">HMS</a>
        <ul class="navbar-nav">
            <li class="nav-item mr-2">
                <a class="nav-link" href="#"><img src="./img/post.svg" alt="" height="23"></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Learn More</a>
            </li>
        </ul>
        </div>
    </nav>
    </section>

    <section id="video1">
        <div class="container-xl pt-5">
            <div class="row my-5 px-3"><div class="h1 font-weight-bold">FEX Livestream</div></div>
            <div class="row pt-5 d-flex justify-content-center">
                <div class="video-container">
                    <iframe src="https://www.youtube.com/embed/4SZEDBFPpgw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>

    <section id="contact">
        <div class="skew container-xl py-5">
            <div class="row py-5">
                <div class="col text-center">
                    <a href="#"><i class="fab fa-youtube fa-3x"></i></a>
                </div>
                <div class="col text-center">
                    <a href="#"><i class="fab fa-instagram fa-3x"></i></a>
                </div>
                <div class="col text-center">
                    <a href="#"><i class="fab fa-whatsapp fa-3x"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section id="footer">
        <div class="container-xl py-5 px-4">
            <div class="row">
                <div class="col-lg-6 margin">
                    <div class="h4">About Us</div>
                    <div class="h6">Halmahera Music School</div>
                    <div >A Music School located in Semarang City with Yamaha International Curriculum</div>
                    <div class="social-media margin">
                        <i class="fab fa-instagram fa-2x"></i>
                        <i class="fab fa-twitter fa-2x"></i>
                        <i class="fab fa-facebook-square fa-2x"></i>
                        <i class="fab fa-youtube fa-2x"></i>
                        <a href="https://hmssemarang.com" target="_blank"><i class="fab fa-firefox fa-2x"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 mt-5">
                    <div class="back mb-5 lead"><a href="#nav">Back to Top <i class="fas fa-arrow-to-top pl-2"></i></a></div>
                    <p>© 2020 Halmahera Music School Semarang. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery and JS bundle w/ Popper.js -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</body>
</html>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/main/style_main.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link href="https://fonts.googleapis.com/css2?family=Titillium+Web&display=swap" rel="stylesheet">

    <title>Main Page</title>
  </head>
  <body>

  <section id="navbar">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand ml-5" href="#">Paseo Smart for Everyday Softness</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto mr-5">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Learn More..</a>
                </li>
                </ul>
            </div>
        </nav>
  </section>

  <section id="hero">
    <img src="/img/hero_main.png" alt="">
  </section>

  <section id="explore">
    <a href="/home/register"><button type="button">Explore</button></a>
    <a href="/home/register"><button type="button">Live Stream</button></a>

    <form action="/login/login" method="post">
        <div class="form-group input">
            <input type="text" class="form-control" name="email_username" id="email_username" placeholder="Looking for something ? ">
        </div>                    
    </form>

  </section>

  <section id="school">
  <h1>Schools</h1>

      <div class="row">
          <div class="col-md-3">
            <div class="card">  
                <img src="/img/sekolah 1.png" class="card-img-top" alt="...">
                <div class="card-body pt-4">
                    <h5 class="card-title">SD Negeri<br> Sang Utara 01</h5>
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">  
                <img src="/img/sekolah 2.png" class="card-img-top" alt="...">
                <div class="card-body pt-4">
                    <h5 class="card-title">SD Negeri<br> Sang Utara 01</h5>
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
                <img src="/img/sekolah 3.png" class="card-img-top" alt="...">
                <div class="card-body pt-4">
                    <h5 class="card-title">SD Negeri<br> Sang Utara 01</h5>
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
                <img src="/img/sekolah 4.png" class="card-img-top" alt="...">
                <div class="card-body pt-4">
                    <h5 class="card-title">SD Negeri<br> Sang Utara 01</h5>
                </div>
            </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-3">
            <div class="card">  
                <img src="/img/sekolah 5.png" class="card-img-top" alt="...">
                <div class="card-body pt-4">
                    <h5 class="card-title">SD Negeri<br> Sang Utara 01</h5>
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">  
                <img src="/img/sekolah 6.png" class="card-img-top" alt="...">
                <div class="card-body pt-4">
                    <h5 class="card-title">SD Negeri<br> Sang Utara 01</h5>
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
                <img src="/img/sekolah 1.png" class="card-img-top" alt="...">
                <div class="card-body pt-4">
                    <h5 class="card-title">SD Negeri<br> Sang Utara 01</h5>
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
                <img src="/img/sekolah 2.png" class="card-img-top" alt="...">
                <div class="card-body pt-4">
                    <h5 class="card-title">SD Negeri<br> Sang Utara 01</h5>
                </div>
            </div>
          </div>
      </div>

  </section>

  <section id="sponsor">
    <h1>Supported By</h1>
    <img src="/img/partner.png" alt="">
  </section>

  <section id="footer">
      <div class="row">
          <div class="col-md-6 kiri">
                <h3>About Us</h3>
                <h5>Halmahera Music School</h5>
                <p>A Music School located in Semarang City with Yamaha <br>International Curriculum</p>
                <div class="social-media">
                    <i class="fab fa-instagram fa-3x"></i>
                    <i class="fab fa-twitter fa-3x"></i>
                    <i class="fab fa-facebook-square fa-3x"></i>
                    <i class="fab fa-youtube fa-3x"></i>
                    <a href="">hmssemarang.com</a>
                </div>
          </div>
          <div class="col-md-6 kanan">
              <a href="">Back to Top <i class="fas fa-arrow-to-top pl-2"></i></a>
              <p>© 2020 Halmahera Music School Semarang. All Rights Reserved.</p>
          </div>
      </div>
  </section>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>php echo school name</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/schoolProfile.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
</head>
<body>
    <section id="nav">
    <nav class="navbar navbar-expand navbar-light bg-white">
        <div class="container-xl d-flex justify-content-between">
        <a class="navbar-brand d-none d-sm-block h1 my-0 font-weight-bold" href="#">Halmahera Music School</a>
        <a class="navbar-brand d-block d-sm-none h my-0 font-weight-bold" href="#">HMS</a>
        <ul class="navbar-nav">
            <li class="nav-item mr-2">
                <a class="nav-link" href="#"><img src="./img/post.svg" alt="" height="23"></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Learn More</a>
            </li>
        </ul>
        </div>
    </nav>
    </section>

    <section id="mainImg">
        <div class="container-fluid">
            <div class="row">
                <img src="./img/schoolPicture.jpg" alt="">
            </div>
        </div>
    </section>

    <section id="schoolProfile">
        <div class="container-xl my-5">
            <div class="row">
                <div class="col-md-6 my-5 d-flex justify-content-center">
                    <img src="./img/schoolLogo.png" alt="" width="300" height="300">
                </div>
                <div class="col-md-6 my-5 d-flex flex-column justify-content-center">
                    <div class="h1 mb-3">School Name</div>
                    <div class="h5 mb-4">Jl. Alamat Alamat no.x kelurahan, kecamatan, kota</div>
                    <p class="lead">
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consectetur magnam pariatur deserunt 
                        a dolores minus. Quasi, tempora. Saepe, eaque. Autem quo placeat cumque provident numquam exercitationem quidem 
                        fuga sit debitis! Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptas, reprehenderit. Quasi numquam
                         inventore, odit eos autem omnis iure est illum.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="extrakulikuler">
        <div class="container-xl px-4 my-5 mx-auto">
            <div class="row">
                <div class="col-md-6 mb-5 d-flex flex-column justify-content-center">
                    <div class="h3 mb-4">Kegiatan dan Ekstrakulikuler</div>
                        <ol>
                            <li class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                            <li class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                            <li class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                            <li class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                            <li class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                        </ol>
                </div>
                <div class="col-md-6 d-flex justify-content-center">
                    <img src="./img/schoolLogo.png" alt="" width="300" height="300">
                </div>
            </div>
        </div>
    </section>

    <section id="fasilitas">
        <div class="container-xl d-flex justify-content-center my-5 pt-5">
            <div class="row">
                <div class="h1">Fasilitas</div>
            </div>
        </div>
        <div class="container-xl">
        <div class="row my-5">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <img class="d-block w-100" src="./img/carousel.jpg" alt="First slide">
                        <div class="carousel-caption">
                            <h5>Fasilitas 1</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img class="d-block w-100" src="./img/carousel 2.jpg" alt="Second slide">
                        <div class="carousel-caption">
                            <h5>Fasilitas 2</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img class="d-block w-100" src="./img/carousel 3.jpg" alt="Third slide">
                        <div class="carousel-caption">
                            <h5>Fasilitas 3</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                      </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
            </div>
        </div>
    </section>

    <section id="prestasi">
        <div class="container-xl px-4 my-5 mx-auto">
            <div class="row">
                <div class="col-md-6 my-5 d-flex justify-content-center">
                    <img src="./img/schoolLogo.png" alt="" width="300" height="300">
                </div>
                <div class="col-md-6 mb-5 my-4 d-flex flex-column justify-content-center">
                    <div class="h2 mb-4">Prestasi</div>
                        <ol>
                            <li class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                            <li class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                            <li class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                            <li class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                            <li class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                        </ol>
                </div>
            </div>
        </div>
    </section>

    <section id="contact">
        <div class="skew container-xl py-5">
            <div class="row py-5">
                <div class="h4 mx-auto">Tertarik? Hubungi Kami!</div>
            </div>
            <div class="row py-5">
                <div class="col text-center">
                    <a href="#" style="color: white;"><i class="fab fa-whatsapp fa-3x"></i></a>
                </div>
                <div class="col text-center">
                    <a href="#" style="color: white;"><i class="far fa-envelope fa-3x"></i></a>
                </div>
                <div class="col text-center">
                    <a href="#" style="color: white;"><i class="fab fa-skype fa-3x"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section id="footer">
        <div class="container-xl py-5 px-4">
            <div class="row">
                <div class="col-lg-6 margin">
                    <div class="h4">About Us</div>
                    <div class="h6">Halmahera Music School</div>
                    <div >A Music School located in Semarang City with Yamaha International Curriculum</div>
                    <div class="social-media margin">
                        <i class="fab fa-instagram fa-2x"></i>
                        <i class="fab fa-twitter fa-2x"></i>
                        <i class="fab fa-facebook-square fa-2x"></i>
                        <i class="fab fa-youtube fa-2x"></i>
                        <a href="https://hmssemarang.com" target="_blank"><i class="fab fa-firefox fa-2x"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 mt-5">
                    <div class="back mb-5 lead"><a href="#nav">Back to Top <i class="fas fa-arrow-to-top pl-2"></i></a></div>
                    <p>© 2020 Halmahera Music School Semarang. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery and JS bundle w/ Popper.js -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</body>
</html>
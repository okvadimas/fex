<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>php echo school name</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/notificationVendor.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
</head>
<body class="d-flex flex-column">
    <section id="nav">
    <nav class="navbar navbar-expand navbar-light bg-white">
        <div class="container-xl d-flex justify-content-between">
        <a class="navbar-brand d-none d-sm-block h1 my-0 font-weight-bold" href="#">Halmahera Music School</a>
        <a class="navbar-brand d-block d-sm-none h my-0 font-weight-bold" href="#">HMS</a>
        <ul class="navbar-nav">
            <li class="nav-item mr-2">
                <a class="nav-link" href="#"><i class="far fa-bell fa-2x"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Learn More</a>
            </li>
        </ul>
        </div>
    </nav>
    </section>

    <section id="notifications" class="flex-grow-1">
        <div class="container-xl my-5">
            <div class="row"><div class="h1 font-weight-bold px-4">Notifications</div></div>


            <div class="row mt-5">
                <div class="container-xl rounded bg-light p-4">
                    <div class="row">
                        <div class="col-sm-2 d-flex flex-column align-items-center">
                            <i class="far fa-user fa-3x mb-2"></i>
                            <p>Username</p>
                        </div>
                        <div class="col-sm-10">
                            <div class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo incidunt non aliquid fugiat eius nam at odit, quis, saepe ex qui distinctio eaque iusto hic, quidem ut maiores explicabo illum.</div>
                        </div>
                    </div>
                    <div class="row px-lg-4 mt-3">
                        <div class="col-sm-6">
                            <div class="p">user@mail.com | +62 8904 3904 323</div>
                        </div>
                        <div class="col-sm-6 d-sm-flex justify-content-end mt-3 mt-sm-0">
                            <p>12:37 7/12/2020</p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row mt-5">
                <div class="container-xl rounded bg-light p-4">
                    <div class="row">
                        <div class="col-sm-2 d-flex flex-column align-items-center">
                            <i class="far fa-user fa-3x mb-2"></i>
                            <p>Username</p>
                        </div>
                        <div class="col-sm-10">
                            <div class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo incidunt non aliquid fugiat eius nam at odit, quis, saepe ex qui distinctio eaque iusto hic, quidem ut maiores explicabo illum.</div>
                        </div>
                    </div>
                    <div class="row px-lg-4 mt-3">
                        <div class="col-sm-6">
                            <div class="p">user@mail.com | +62 8904 3904 323</div>
                        </div>
                        <div class="col-sm-6 d-sm-flex justify-content-end mt-3 mt-sm-0">
                            <p>12:37 7/12/2020</p>
                        </div>
                    </div>
                </div>
            </div>

            
            <div class="row mt-5">
                <div class="container-xl rounded bg-light p-4">
                    <div class="row">
                        <div class="col-sm-2 d-flex flex-column align-items-center">
                            <i class="far fa-user fa-3x mb-2"></i>
                            <p>Username</p>
                        </div>
                        <div class="col-sm-10">
                            <div class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo incidunt non aliquid fugiat eius nam at odit, quis, saepe ex qui distinctio eaque iusto hic, quidem ut maiores explicabo illum.</div>
                        </div>
                    </div>
                    <div class="row px-lg-4 mt-3">
                        <div class="col-sm-6">
                            <div class="p">user@mail.com | +62 8904 3904 323</div>
                        </div>
                        <div class="col-sm-6 d-sm-flex justify-content-end mt-3 mt-sm-0">
                            <p>12:37 7/12/2020</p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>

    <section id="footer" class="mt-5">
        <div class="container-xl py-5 px-4">
            <div class="row">
                <div class="col-lg-6 margin">
                    <div class="h4">About Us</div>
                    <div class="h6">Halmahera Music School</div>
                    <div >A Music School located in Semarang City with Yamaha International Curriculum</div>
                    <div class="social-media margin">
                        <i class="fab fa-instagram fa-2x"></i>
                        <i class="fab fa-twitter fa-2x"></i>
                        <i class="fab fa-facebook-square fa-2x"></i>
                        <i class="fab fa-youtube fa-2x"></i>
                        <a href="https://hmssemarang.com" target="_blank"><i class="fab fa-firefox fa-2x"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 mt-5">
                    <div class="back mb-5 lead"><a href="#nav">Back to Top <i class="fas fa-arrow-to-top pl-2"></i></a></div>
                    <p>© 2020 Halmahera Music School Semarang. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery and JS bundle w/ Popper.js -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</body>
</html>
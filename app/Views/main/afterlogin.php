<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Titillium+Web&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

    <title>Main Page</title>
  </head>
  <body>

    <section id="nav">
        <nav class="navbar navbar-expand-sm navbar-dark">
            <div class="container-xl">
                <a class="navbar-brand d-none d-sm-block" href="#"><h3>Halmahera Music School</h3></a>
                <a class="navbar-brand d-block d-sm-none" href="#"><h3>HMS</h3></a>
                  <ul class="navbar-nav ml-auto d-flex">
                    <li class="nav-item py-auto pb-2">
                        <a class="nav-link" href="#"><img src="./img/post.svg" style="filter: invert();" alt="" height="25"></a>
                    </li>
                    <li class="nav-item d-none d-sm-block">
                        <a class="nav-link text-white text-decoration-none" href="#"><h5>Learn More</h5></a></a>
                    </li>
                  </ul>
            </div>
        </nav>
    </section>

    <section id="hero">
        <div class="container-xl">
            <div class="row">
                <img class="d-none d-sm-block" src="/img/Hero_main.png" alt="" width="100%">
                <img class="d-block d-sm-none" src="/img/Hero.png" alt="" width="100%">
            </div>
        </div>
    </section>

    <section id="register">
        <div class="container-fluid pb-5 pt-5">
            <div class="row d-flex justify-content-center">
                <a class="mr-sm-4" href="/main/explore"><button type="button" class="bg-white"><h6>Explore</h6></button></a>
                <a class="mt-3 mt-sm-0 ml-sm-4" href="/main/livestream"><button class="bg-white" type="button"><h6>Live Stream</h6></button></a>
            </div>
            <div class="row d-flex justify-content-center mt-4 mt-sm-5">
                <div class="col-sm-12">
                    <form action="/login/login" method="post">
                        <div class="form-group d-flex justify-content-center pr-3 pr-sm-0">
                            <input autocomplete="off" style="width: 1000px; border-radius: 20px; padding-left: 20px;" type="text" class="form-control border-0" name="email_username" id="email_username" placeholder="Looking for something ? ">
                            <i class="fas fa-search fa-1x" style="margin-left: -30px; margin-top: 10px;"></i>
                        </div>                    
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="schools">
        <div class="container-xl">
            <div class="row text-center pb-5">
                <div class="col-sm-12"><h3 class="font-weight-bolder">Schools</h3></div>
            </div> 
            <div class="row pt-5">
                <div class="col-lg-4 pb-4 px-5">
                    <div class="card rounded-lg border-0 px-5">
                        <img src="/img/sekolah 5.png" class="card-img-top w-100" alt="...">
                        <div class="card-body text-center">
                          <p class="card-text"><h5><a href="/main/schoolProfile" class="text-decoration-none text-dark">Sekolah Negeri Sang Utara 01</a></h5></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 pb-4 px-5">
                    <div class="card rounded-lg border-0 px-5">
                        <img src="/img/sekolah 5.png" class="card-img-top w-100" alt="...">
                        <div class="card-body text-center">
                        <p class="card-text"><h5><a href="" class="text-decoration-none text-dark">Sekolah Negeri Sang Utara 01</a></h5></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 pb-4 px-5">
                    <div class="card rounded-lg border-0 px-5">
                        <img src="/img/sekolah 5.png" class="card-img-top w-100" alt="...">
                        <div class="card-body text-center">
                        <p class="card-text"><h5><a href="" class="text-decoration-none text-dark">Sekolah Negeri Sang Utara 01</a></h5></p>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="row pt-5 d-none d-lg-flex">
                <div class="col-lg-4 pb-4 px-5">
                    <div class="card rounded-lg border-0 px-5">
                        <img src="/img/sekolah 5.png" class="card-img-top w-100" alt="...">
                        <div class="card-body text-center">
                        <p class="card-text"><h5><a href="" class="text-decoration-none text-dark">Sekolah Negeri Sang Utara 01</a></h5></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 pb-4 px-5">
                    <div class="card rounded-lg border-0 px-5">
                        <img src="/img/sekolah 5.png" class="card-img-top w-100" alt="...">
                        <div class="card-body text-center">
                        <p class="card-text"><h5><a href="" class="text-decoration-none text-dark">Sekolah Negeri Sang Utara 01</a></h5></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 pb-4 px-5">
                    <div class="card rounded-lg border-0 px-5">
                        <img src="/img/sekolah 5.png" class="card-img-top w-100" alt="...">
                        <div class="card-body text-center">
                        <p class="card-text"><h5><a href="" class="text-decoration-none text-dark">Sekolah Negeri Sang Utara 01</a></h5></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="sponsor">
        <div class="container-xl mt-3">
            <div class="row text-center pt-5">
                <div class="col-sm-12"><h3 class="font-weight-bolder">Sponsors</h3></div>
            </div>
            <div class="row pt-3">
                <img src="/img/partner.png" alt="" width="100%">
            </div>
        </div>
    </section>

    <section id="contact">
        <div class="skew container-xl py-5">
            <div class="row py-5">
                <div class="h4 mx-auto">Tertarik? Hubungi Kami!</div>
            </div>
            <div class="row py-5">
                <div class="col text-center">
                    <a href="#" style="color: white;"><i class="fab fa-whatsapp fa-3x"></i></a>
                </div>
                <div class="col text-center">
                    <a href="#" style="color: white;"><i class="far fa-envelope fa-3x"></i></a>
                </div>
                <div class="col text-center">
                    <a href="#" style="color: white;"><i class="fab fa-skype fa-3x"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section id="footer">
        <div class="container-xl py-5 px-4">
            <div class="row">
                <div class="col-lg-6 margin">
                    <div class="h4">About Us</div>
                    <div class="h6">Halmahera Music School</div>
                    <div >A Music School located in Semarang City with Yamaha International Curriculum</div>
                    <div class="social-media margin">
                        <i class="fab fa-instagram fa-2x"></i>
                        <i class="fab fa-twitter fa-2x"></i>
                        <i class="fab fa-facebook-square fa-2x"></i>
                        <i class="fab fa-youtube fa-2x"></i>
                        <a href="https://hmssemarang.com" target="_blank"><i class="fab fa-firefox fa-2x"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 mt-5">
                    <div class="back mb-5 lead"><a href="#nav">Back to Top <i class="fas fa-arrow-to-top pl-2"></i></a></div>
                    <p>© 2020 Halmahera Music School Semarang. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>
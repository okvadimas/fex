<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Titillium+Web&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

    <title>Login Page</title>
  </head>
  <body>

    <section id="nav">
        <nav class="navbar navbar-expand-sm navbar-dark">
            <div class="container-xl">
                <a class="navbar-brand d-none d-sm-block" href="#"><h3>Halmahera Music School</h3></a>
                <a class="navbar-brand d-block d-sm-none" href="#"><h3>HMS</h3></a>
                  <ul class="navbar-nav ml-auto d-flex">
                    <li class="nav-item py-auto pb-2">
                        <a class="nav-link" href="#"><img src="./img/post.svg" style="filter: invert();" alt="" height="25"></a>
                    </li>
                    <li class="nav-item d-none d-sm-block">
                        <a class="nav-link text-white text-decoration-none" href="#"><h5>Learn More</h5></a></a>
                    </li>
                  </ul>
            </div>
        </nav>
    </section>

    <section id="hero">
        <div class="container-xl">
            <div class="row">
                <div class="col-md-6 d-flex justify-content-center my-5">
                    <img src="./img/Rectangle 12.png" alt="" width="300" height="300">
                </div>
                <div class="col-md-6 my-5">
                    <h1 class="mb-3">Maria Regina School</h1>
                    <h5 class="mb-4">Jl. Alamat Alamat no.x kelurahan, kecamatan, kota</h5>
                    <p class="lead">
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consectetur magnam pariatur deserunt 
                        a dolores minus. Quasi, tempora. Saepe, eaque. Autem quo placeat cumque provident numquam exercitationem quidem 
                        fuga sit debitis! Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptas, reprehenderit. Quasi numquam
                         inventore, odit eos autem omnis iure est illum.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="fasilitas">
        <div class="container-xl my-5">
            <div class="row d-flex justify-content-center">
                <h3>Fasilitas</h3>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-md-4 mt-5 my-sm-5">
                    <div class="card py-2" style="border-radius: 20px;" >
                        <div class="card-text d-flex justify-content-center h5">Kolam Renang</div>
                    </div>
                </div>
                <div class="col-md-4 my-2 my-sm-5">
                    <div class="card py-2" style="border-radius: 20px;">
                        <div class="card-text d-flex justify-content-center h5">Lapangan Sepak Bola</div>
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-center my-5 my-sm-3">
                <a href="" class="h5 text-dark text-decoration-none">Lihat Selengkapnya...</a>
            </div>
        </div>
    </section>

    <section id="suggest">
        <div class="container-xl my-5 py-5">
            <div class="row d-flex justify-content-center">
                <h3>Mungkin anda tertarik dengan </h3>
            </div>
            <div class="row d-flex justify-content-center my-5">
                <div class="col-md-3 d-flex justify-content-center my-4 my-sm-0">
                    <img src="img/1.png" alt="" class="img-fluid">
                </div>
                <div class="col-md-3 d-flex justify-content-center my-4 my-sm-0">
                    <img src="img/2.png" alt="" class="img-fluid">
                </div>
                <div class="col-md-3 d-flex justify-content-center my-4 my-sm-0">
                    <img src="img/3.png" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <section id="footer">
        <div class="container-xl py-5 px-4">
            <div class="row">
                <div class="col-lg-6 margin">
                    <div class="h4">About Us</div>
                    <div class="h6">Halmahera Music School</div>
                    <div >A Music School located in Semarang City with Yamaha International Curriculum</div>
                    <div class="social-media margin">
                        <i class="fab fa-instagram fa-2x"></i>
                        <i class="fab fa-twitter fa-2x"></i>
                        <i class="fab fa-facebook-square fa-2x"></i>
                        <i class="fab fa-youtube fa-2x"></i>
                        <a href="https://hmssemarang.com" target="_blank"><i class="fab fa-firefox fa-2x"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 mt-5">
                    <div class="back mb-5 lead"><a href="#nav">Back to Top <i class="fas fa-arrow-to-top pl-2"></i></a></div>
                    <p>© 2020 Halmahera Music School Semarang. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>
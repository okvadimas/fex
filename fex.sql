-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2020 at 12:25 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fex`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail`
--

CREATE TABLE `detail` (
  `id` int(11) NOT NULL,
  `nama_sekolah` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `fasilitas` varchar(255) NOT NULL,
  `ig_stream` varchar(255) NOT NULL,
  `yt_stream` varchar(255) NOT NULL,
  `zoom_stream` varchar(255) NOT NULL,
  `prestasi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `detail`
--

INSERT INTO `detail` (`id`, `nama_sekolah`, `desc`, `alamat`, `fasilitas`, `ig_stream`, `yt_stream`, `zoom_stream`, `prestasi`) VALUES
(1, 'Sekolah Negeri 1', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 1', 'Fasilitas Sekolah Negeri 1', '6288211234125', 'gPFqyLYf1DU', 'Zoom Stream Sekolah Negeri 1', 'Prestasi Sekolah Negeri 1'),
(2, 'Sekolah Negeri 2', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 2', 'Fasilitas Sekolah Negeri 2', '6288211234125', 'FzcfZyEhOoI', 'Zoom Stream Sekolah Negeri 2', 'Prestasi Sekolah Negeri 2'),
(3, 'Sekolah Negeri 3', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 3', 'Fasilitas Sekolah Negeri 3', '6288211234125', 'q-g7BPdSmP4', 'Zoom Stream Sekolah Negeri 3', 'Prestasi Sekolah Negeri 3'),
(4, 'Sekolah Negeri 4', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 4', 'Fasilitas Sekolah Negeri 4', '6288211234125', 'Youtube Stream Sekolah Negeri 4', 'Zoom Stream Sekolah Negeri 4', 'Prestasi Sekolah Negeri 4'),
(5, 'Sekolah Negeri 5', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 5', 'Fasilitas Sekolah Negeri 5', '6288211234125', 'Youtube Stream Sekolah Negeri 5', 'Zoom Stream Sekolah Negeri 5', 'Prestasi Sekolah Negeri 5'),
(6, 'Sekolah Negeri 6', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 6', 'Fasilitas Sekolah Negeri 6', '6288211234125', 'Youtube Stream Sekolah Negeri 6', 'Zoom Stream Sekolah Negeri 6', 'Prestasi Sekolah Negeri 6'),
(7, 'Sekolah Negeri 7', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 7', 'Fasilitas Sekolah Negeri 7', '088211234125', 'Youtube Stream Sekolah Negeri 7', 'Zoom Stream Sekolah Negeri 7', 'Prestasi Sekolah Negeri 7'),
(8, 'Sekolah Negeri 8', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 8', 'Fasilitas Sekolah Negeri 8', '088211234125', 'Youtube Stream Sekolah Negeri 8', 'Zoom Stream Sekolah Negeri 8', 'Prestasi Sekolah Negeri 8'),
(9, 'Sekolah Negeri 9', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 9', 'Fasilitas Sekolah Negeri 9', 'Instagram Stream Sekolah Negeri 9', 'Youtube Stream Sekolah Negeri 9', 'Zoom Stream Sekolah Negeri 9', 'Prestasi Sekolah Negeri 9'),
(10, 'Sekolah Negeri 10', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 10', 'Fasilitas Sekolah Negeri 10', 'Instagram Stream Sekolah Negeri 10', 'Youtube Stream Sekolah Negeri 10', 'Zoom Stream Sekolah Negeri 10', 'Prestasi Sekolah Negeri 10'),
(11, 'Sekolah Negeri 11', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 11', 'Fasilitas Sekolah Negeri 11', 'Instagram Stream Sekolah Negeri 11', 'Youtube Stream Sekolah Negeri 11', 'Zoom Stream Sekolah Negeri 11', 'Prestasi Sekolah Negeri 11'),
(12, 'Sekolah Negeri 12', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 12', 'Fasilitas Sekolah Negeri 12', 'Instagram Stream Sekolah Negeri 12', 'Youtube Stream Sekolah Negeri 12', 'Zoom Stream Sekolah Negeri 12', 'Prestasi Sekolah Negeri 12'),
(13, 'Sekolah Negeri 13', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 13', 'Fasilitas Sekolah Negeri 13', 'Instagram Stream Sekolah Negeri 13', 'Youtube Stream Sekolah Negeri 13', 'Zoom Stream Sekolah Negeri 13', 'Prestasi Sekolah Negeri 13'),
(14, 'Sekolah Negeri 14', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 14', 'Fasilitas Sekolah Negeri 14', 'Instagram Stream Sekolah Negeri 14', 'Youtube Stream Sekolah Negeri 14', 'Zoom Stream Sekolah Negeri 14', 'Prestasi Sekolah Negeri 14'),
(15, 'Sekolah Negeri 15', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 15', 'Fasilitas Sekolah Negeri 15', 'Instagram Stream Sekolah Negeri 15', 'Youtube Stream Sekolah Negeri 15', 'Zoom Stream Sekolah Negeri 15', 'Prestasi Sekolah Negeri 15'),
(16, 'Sekolah Negeri 16', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 16', 'Fasilitas Sekolah Negeri 16', 'Instagram Stream Sekolah Negeri 16', 'Youtube Stream Sekolah Negeri 16', 'Zoom Stream Sekolah Negeri 16', 'Prestasi Sekolah Negeri 16'),
(17, 'Sekolah Negeri 17', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 17', 'Fasilitas Sekolah Negeri 17', 'Instagram Stream Sekolah Negeri 17', 'Youtube Stream Sekolah Negeri 17', 'Zoom Stream Sekolah Negeri 17', 'Prestasi Sekolah Negeri 17'),
(18, 'Sekolah Negeri 18', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 18', 'Fasilitas Sekolah Negeri 18', 'Instagram Stream Sekolah Negeri 18', 'Youtube Stream Sekolah Negeri 18', 'Zoom Stream Sekolah Negeri 18', 'Prestasi Sekolah Negeri 18'),
(19, 'Sekolah Negeri 19', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 19', 'Fasilitas Sekolah Negeri 19', 'Instagram Stream Sekolah Negeri 19', 'Youtube Stream Sekolah Negeri 19', 'Zoom Stream Sekolah Negeri 19', 'Prestasi Sekolah Negeri 19'),
(20, 'Sekolah Negeri 20', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 20', 'Fasilitas Sekolah Negeri 20', 'Instagram Stream Sekolah Negeri 20', 'Youtube Stream Sekolah Negeri 20', 'Zoom Stream Sekolah Negeri 20', 'Prestasi Sekolah Negeri 20'),
(21, 'Sekolah Negeri 21', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 21', 'Fasilitas Sekolah Negeri 21', 'Instagram Stream Sekolah Negeri 21', 'Youtube Stream Sekolah Negeri 21', 'Zoom Stream Sekolah Negeri 21', 'Prestasi Sekolah Negeri 21'),
(22, 'Sekolah Negeri 22', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 22', 'Fasilitas Sekolah Negeri 22', 'Instagram Stream Sekolah Negeri 22', 'Youtube Stream Sekolah Negeri 22', 'Zoom Stream Sekolah Negeri 22', 'Prestasi Sekolah Negeri 22'),
(23, 'Sekolah Negeri 23', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 23', 'Fasilitas Sekolah Negeri 23', 'Instagram Stream Sekolah Negeri 23', 'Youtube Stream Sekolah Negeri 23', 'Zoom Stream Sekolah Negeri 23', 'Prestasi Sekolah Negeri 23'),
(24, 'Sekolah Negeri 24', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 24', 'Fasilitas Sekolah Negeri 24', 'Instagram Stream Sekolah Negeri 24', 'Youtube Stream Sekolah Negeri 24', 'Zoom Stream Sekolah Negeri 24', 'Prestasi Sekolah Negeri 24'),
(25, 'Sekolah Negeri 25', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 25', 'Fasilitas Sekolah Negeri 25', 'Instagram Stream Sekolah Negeri 25', 'Youtube Stream Sekolah Negeri 25', 'Zoom Stream Sekolah Negeri 25', 'Prestasi Sekolah Negeri 25'),
(26, 'Sekolah Negeri 26', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 26', 'Fasilitas Sekolah Negeri 26', 'Instagram Stream Sekolah Negeri 26', 'Youtube Stream Sekolah Negeri 26', 'Zoom Stream Sekolah Negeri 26', 'Prestasi Sekolah Negeri 26'),
(27, 'Sekolah Negeri 27', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 27', 'Fasilitas Sekolah Negeri 27', 'Instagram Stream Sekolah Negeri 27', 'Youtube Stream Sekolah Negeri 27', 'Zoom Stream Sekolah Negeri 27', 'Prestasi Sekolah Negeri 27'),
(28, 'Sekolah Negeri 28', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 28', 'Fasilitas Sekolah Negeri 28', 'Instagram Stream Sekolah Negeri 28', 'Youtube Stream Sekolah Negeri 28', 'Zoom Stream Sekolah Negeri 28', 'Prestasi Sekolah Negeri 28'),
(29, 'Sekolah Negeri 29', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 29', 'Fasilitas Sekolah Negeri 29', 'Instagram Stream Sekolah Negeri 29', 'Youtube Stream Sekolah Negeri 29', 'Zoom Stream Sekolah Negeri 29', 'Prestasi Sekolah Negeri 29'),
(30, 'Sekolah Negeri 30', 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', 'Jalan Sekolah Negeri 30', 'Fasilitas Sekolah Negeri 30', 'Instagram Stream Sekolah Negeri 30', 'Youtube Stream Sekolah Negeri 30', 'Zoom Stream Sekolah Negeri 30', 'Prestasi Sekolah Negeri 30');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`) VALUES
(1, 'dimas', 'dimasokva@gmail.com', '$2y$10$np3seNqgsxmQYXg5DNUfSuLbFPF1Uw.n6cYhtSzDjA4IMhk4RfZMS'),
(2, 'okva', 'okvadimas@gmail.com', '$2y$10$xg6VuDSxjNAdnQyxW0sSgOfyUtJbYVf.gR3Ze0JTKJvkOyu5TcBoC');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail`
--
ALTER TABLE `detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail`
--
ALTER TABLE `detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
